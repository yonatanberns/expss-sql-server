/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
// import user_model from "./user.model.mjs";
import express from 'express';
import log from '@ajar/marker';
import { schemaForInsert, schemaForPatch } from "./user.validition.mjs";
import { sqlConnection } from '../../db/mysql.connection.mjs'


const router = express.Router();

console.log('in user.router');

// parse json req.body on post routes
router.use(express.json())

const validateInsert = (req, res, next) => {
  console.log('in middleware validateInsert');
  // console.log('in val insert', req.body);
  const resVal = schemaForInsert.validate(req.body);
  if(!'error' in resVal) {
    next();
  } else {
    next(resVal.error)
  }
}

const validatePatch = (req, res, next) => {
  console.log('in middleware validatePatch');
  const resVal = schemaForPatch.validate(req.body);
  if(!'error' in resVal) {
    next();
  } else {
    next(resVal.error)
  }
}

// Add new user
router.post("/",validateInsert, raw(async (req, res) => {
    log.obj(req.body, "create a user, req.body:");

    // const user = await user_model.create(req.body);
    const [rows,fields] = await sqlConnection.query(`INSERT INTO users (first_name, last_name, email, phone)
    VALUES ('${req.body.first_name}','${req.body.last_name}','${req.body.email}','${req.body.phone}');`);

    res.status(200).json(rows);
}));


// GET ALL USERS
router.get( "/",raw(async (req, res) => {

    const [rows,fields] = await sqlConnection.query('SELECT * FROM users');
    res.status(200).json(rows);
  })
);


router.get('/paginate/:page?/:items?', raw( async(req, res)=> {

  log.obj(req.params, "get all users, req.params:");
  let { page = 0 ,items = 10 } = req.params;

  const [rows, fields] = await sqlConnection.query(`
          SELECT 
            *
        FROM
            users
           
        LIMIT ${items * page}, ${items};
  `);
               
  res.status(200).json(rows)

}))

// GETS A SINGLE USER
router.get("/:id",raw(async (req, res) => {

   const [rows,fields] = await sqlConnection.query(`SELECT id,first_name,last_name,email,phone FROM users Where id=${req.params.id}`);

    if (!rows) return res.status(404).json({ status: "No user found." });
    res.status(200).json(rows);
  })
);

// UPDATES A SINGLE USER
router.put("/:id", validatePatch, raw(async (req, res) => {

  console.log('in put->', req.body);

    const [rows,fields] = await sqlConnection.query(
    `UPDATE users
    SET first_name='${req.body.first_name}', last_name='${req.body.last_name}',
    email='${req.body.email}', phone='${req.body.phone}'
    WHERE id=${req.params.id}`);

    res.status(200).json(rows);
  })
);


// DELETES A USER
router.delete("/:id",raw(async (req, res) => {
    // const user = await user_model.findByIdAndRemove(req.params.id);
   const [rows,fields] = await sqlConnection.query(`DELETE FROM users WHERE id=${req.params.id}`);

    res.status(200).json(rows);
  })
);

export default router;
