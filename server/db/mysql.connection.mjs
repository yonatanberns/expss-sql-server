// import mongoose from 'mongoose'
// import log      from '@ajar/marker'

// export const connect_db = async uri => {
//     const options = {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//     };
//     await mongoose.connect(uri,options)
//     log.magenta(' ✨  Connected to Mysql ✨ ')
// }

import mysql from 'mysql2/promise'
// export const connect_db = async ()=> {

   // create the connection
  export const sqlConnection = await mysql.createConnection({host:'localhost', user: 'root', database: 'playground', port: 3307, password: 'mysecret'});
  // query database
//   const [rows, fields] = await connection.execute('SELECT * FROM `table` WHERE `name` = ? AND `age` > ?', 
//                         ['Morty', 14]);
// }